package fr.discowzombie.forcepack;

import fr.discowzombie.forcepack.command.PackCommand;
import fr.discowzombie.forcepack.listener.PackListener;
import fr.discowzombie.forcepack.service.ResourcePackService;
import fr.discowzombie.forcepack.utils.EnvUtils;
import fr.discowzombie.forcepack.utils.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;
import org.codehaus.plexus.util.IOUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.CheckReturnValue;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public final class ForcePack extends JavaPlugin {

    public static @NotNull String PACK_URLS_FILENAME = "pack_urls";

    @Nullable
    private ResourcePackService resourcePackService = null;

    @Override
    public void onEnable() {
        final var packUrlOpt = this.readPackUrl();

        if (packUrlOpt.isPresent() && !packUrlOpt.get().isEmpty()) {
            this.getLogger().info("Resource-pack URL is: " + packUrlOpt.get());

            this.resourcePackService = new ResourcePackService(packUrlOpt.get().toArray(new String[0]));
            this.resourcePackService.startScheduling();

            Bukkit.getServicesManager().register(ResourcePackService.class, this.resourcePackService, this, ServicePriority.Normal);

            Bukkit.getPluginManager().registerEvents(new PackListener(), this);

            Objects.requireNonNull(getCommand("pack")).setExecutor(new PackCommand());
        } else {
            this.getLogger().info("No resource-pack available.");
        }
    }

    @Override
    public void onDisable() {
        if (this.resourcePackService != null) {
            this.resourcePackService.stopScheduling();
            this.resourcePackService = null;
        }
    }

    // TODO patch le mauvais pack envoyé

    @NotNull
    @CheckReturnValue
    private Optional<List<String>> readPackUrl() {
        // Try to read from env
        var packUrls = EnvUtils.getResourcePackUrls();
        if (!packUrls.isEmpty())
            return Optional.of(packUrls);

        // Try to read from file
        // Create the plugin data folder
        if (!super.getDataFolder().exists())
            if (!super.getDataFolder().mkdirs())
                throw new RuntimeException("Cannot create plugin data folder.");

        final var packUrlFile = new File(this.getDataFolder(), PACK_URLS_FILENAME);

        try {
            packUrls = FileUtils.readLinesFromFile(packUrlFile);
            if (!packUrls.isEmpty())
                return Optional.of(packUrls);
        } catch (IOException ignored) {}

        return Optional.empty();
    }
}
