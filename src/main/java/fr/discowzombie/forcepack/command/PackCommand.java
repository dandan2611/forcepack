package fr.discowzombie.forcepack.command;

import fr.discowzombie.forcepack.service.ResourcePackService;
import fr.discowzombie.forcepack.utils.PackUtils;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;

public final class PackCommand implements CommandExecutor {

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof final Player player) {
            final ResourcePackService resourcePackService = Objects.requireNonNull(Bukkit.getServicesManager().load(ResourcePackService.class));
            final var infos = resourcePackService.getResourcePackInfos();

            PackUtils.applyResourcePacks(player, infos, false);
        }

        return true;
    }
}
