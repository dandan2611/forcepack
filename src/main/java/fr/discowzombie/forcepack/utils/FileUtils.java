package fr.discowzombie.forcepack.utils;

import org.codehaus.plexus.util.IOUtil;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

public final class FileUtils {

    public static @NotNull List<String> readLinesFromFile(@NotNull File destFile) throws IOException {
        try (final Reader reader = new FileReader(destFile)) {
            final var fullStrings = IOUtil.toString(reader).split("\n");
            return List.of(fullStrings);
        }
    }

}
