package fr.discowzombie.forcepack.utils;

import com.google.common.collect.Lists;
import org.jetbrains.annotations.CheckReturnValue;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public final class EnvUtils {

    @CheckReturnValue
    public static boolean isAutoApplyNewVersion() {
        return System.getenv("FORCEPACK_AUTO_APPLY_NEW_VERSION") != null
                && System.getenv("FORCEPACK_AUTO_APPLY_NEW_VERSION").equals("1");
    }

    @CheckReturnValue
    public static @NotNull List<String> getResourcePackUrls() {
        if (System.getenv("FORCEPACK_PACK_URLS") == null)
            return List.of();
        return List.of(System.getenv("FORCEPACK_PACK_URLS").split(","));
    }

}
