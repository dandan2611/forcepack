package fr.discowzombie.forcepack.utils;

import net.kyori.adventure.resource.ResourcePackInfoLike;
import net.kyori.adventure.resource.ResourcePackRequest;
import net.kyori.adventure.resource.ResourcePackStatus;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.CheckReturnValue;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

public final class PackUtils {

    private static final HttpClient HTTP_CLIENT = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1) // For some reason, HTTP/2 keep streams opened "too many concurrent streams"
            .build();

    private static final SimpleDateFormat RFC822_DATE_FORMAT = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);

    private static final MessageDigest SHA_1_DIGEST;

    static {
        try {
            SHA_1_DIGEST = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private PackUtils() {
    }

    @CheckReturnValue
    @NotNull
    public static CompletableFuture<byte[]> computeSha1Hash(final @NotNull String url, @Nullable Date previousCheck) {
        if (previousCheck == null) {
            previousCheck = new Date(0);
        }

        final var request = HttpRequest.newBuilder(URI.create(url))
                .GET()
                .header("If-Modified-Since", RFC822_DATE_FORMAT.format(previousCheck))
                .build();

        return HTTP_CLIENT.sendAsync(request, HttpResponse.BodyHandlers.ofInputStream())
                .thenApplyAsync(response -> {
                    try {
                        final var statusCode = response.statusCode();

                        if (304 == statusCode) { // No change since the last time
                            return new byte[0];
                        }  else if (200 != statusCode) { // Failed :(
                            throw new CompletionException(new Exception("Bad HTTP code returned: " + response.statusCode()));
                        }

                        try (final var inputStream = response.body()) {
                            final byte[] b = inputStream.readAllBytes();
                            return SHA_1_DIGEST.digest(b);
                        }
                    } catch (/*ParseException |*/ IOException e) {
                        throw new CompletionException(e);
                    }
                });
    }

    public static void applyResourcePacks(final @NotNull Player player, final @NotNull List<ResourcePackInfoLike> infos, boolean replace) {
        player.sendResourcePacks(ResourcePackRequest.resourcePackRequest()
                .packs(infos)
                .replace(replace)
                .prompt(Component.text("For a better experience, please accept the provided resource pack(s)", NamedTextColor.GOLD))
                .required(true)
                .callback((uuid, status, audience) -> {
                    if (ResourcePackStatus.DECLINED == status || ResourcePackStatus.FAILED_DOWNLOAD == status) {
                        audience.sendMessage(
                                Component.text("The resource pack could not be installed, please use the command", NamedTextColor.GRAY)
                                        .append(Component.text(" /pack ", NamedTextColor.WHITE).clickEvent(ClickEvent.runCommand("/pack")))
                                        .append(Component.text("to obtain it.", NamedTextColor.GRAY))
                        );
                    }
                })
        );
    }

}
