package fr.discowzombie.forcepack.model;

import fr.discowzombie.forcepack.ForcePack;
import fr.discowzombie.forcepack.utils.PackUtils;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.resource.ResourcePackInfo;
import net.kyori.adventure.resource.ResourcePackInfoLike;
import net.kyori.adventure.resource.ResourcePackRequest;
import org.jetbrains.annotations.CheckReturnValue;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URI;
import java.util.Arrays;
import java.util.Date;
import java.util.HexFormat;
import java.util.UUID;
import java.util.concurrent.*;

public class ResourcePack {

    private final @NotNull UUID id = UUID.randomUUID();

    private final @NotNull String url;

    private byte[] currentHash;

    private @Nullable Date lastCheck;

    private @Nullable ResourcePackInfoLike info;

    /**
     * Create a new resource pack and automatically compute the hash.
     *
     * @param url the url of the resource pack
     */
    public ResourcePack(final @NotNull String url) {
        this.url = url;
        this.currentHash = new byte[0];
        this.lastCheck = null;
        this.refreshHash().exceptionally(e -> { // Soft fail if an error occurred while computing the hash
            ForcePack.getPlugin(ForcePack.class).getLogger().severe("An error occurred while computing the hash of '" + this.url + "' : " + e.getMessage());
            return false;
        });
    }

    /**
     * Refresh the hash of the resource pack.
     *
     * @return a future that will be completed with true if the hash changed, false otherwise. The future will throw if
     * an error occurred while computing the hash.
     */
    @CheckReturnValue
    public CompletableFuture<Boolean> refreshHash() {
        final var now = new Date();

        return PackUtils.computeSha1Hash(this.url, this.lastCheck).thenApply(hash -> {
            if (hash.length == 0) {
                // ForcePack.getPlugin(ForcePack.class).getLogger().info("Resource pack hash is empty for '" + this.url + "'");
                // No need to log since the pack has not been updated
                this.lastCheck = now;
                return false;
            }

            this.lastCheck = now;
            boolean changed = !Arrays.equals(hash, this.currentHash);
            this.currentHash = hash;
            this.info = this.createInfo();
            if (changed) {
                ForcePack.getPlugin(ForcePack.class).getLogger().info("Resource pack hash changed for " + this.url + " : " + this.getCurrentStringHash());
            }
            return changed;
        }).exceptionally(e -> {
            ForcePack.getPlugin(ForcePack.class).getLogger().severe("An error occurred while refreshing the hash of '" + this.url + "' : " + e.getMessage());
            return false;
        });
    }

    /**
     * Check if the resource pack is loaded.
     *
     * @return true if the resource pack is loaded, false otherwise
     */
    public boolean isLoaded() {
        return this.currentHash.length != 0;
    }

    private @NotNull ResourcePackInfo createInfo() {
        final var hash = this.getCurrentStringHash();

        if (hash == null)
            throw new IllegalStateException("Resource pack is not loaded");

        try {
            return ResourcePackInfo.resourcePackInfo(this.id, new URI(this.url), hash);
        } catch (Exception e) {
            throw new IllegalStateException("Unable to create resource pack info", e);
        }
    }

    public @NotNull String getUrl() {
        return url;
    }

    public byte[] getCurrentHash() {
        return currentHash;
    }

    public @Nullable String getCurrentStringHash() {
        return (this.currentHash.length == 0) ? null : new String(this.currentHash);
    }

    public @Nullable ResourcePackInfoLike getInfo() {
        return info;
    }

    /**
     * URL with the hash appended, to fix <a href="https://bugs.mojang.com/browse/MC-164316">MC-164316</a>
     *
     * @return The pack url with the sha appended
     */
    public @NotNull String getHashedUrl() {
        if (!isLoaded())
            return this.url;
        return String.format("%s#%s", this.url, HexFormat.of().formatHex(this.currentHash));
    }

    @Override
    public int hashCode() {
        return this.url.hashCode();
    }

}
