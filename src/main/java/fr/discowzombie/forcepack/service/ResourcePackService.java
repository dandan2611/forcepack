package fr.discowzombie.forcepack.service;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import fr.discowzombie.forcepack.ForcePack;
import fr.discowzombie.forcepack.model.ResourcePack;
import fr.discowzombie.forcepack.utils.EnvUtils;
import fr.discowzombie.forcepack.utils.PackUtils;
import it.unimi.dsi.fastutil.Pair;
import net.kyori.adventure.resource.ResourcePackInfoLike;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ResourcePackService {

    private final @NotNull JavaPlugin plugin;

    private final @NotNull Set<ResourcePack> packs;

    private @Nullable BukkitTask refreshTask = null;

    public ResourcePackService(final @NotNull String... urls) {
        this.packs = new LinkedHashSet<>();
        this.plugin = ForcePack.getPlugin(ForcePack.class);

        for (String url : urls)
            this.packs.add(new ResourcePack(url));
    }

    @CanIgnoreReturnValue
    public boolean startScheduling() {
        if (this.refreshTask != null)
            return false;
        this.refreshTask = Bukkit.getScheduler().runTaskTimerAsynchronously(this.plugin, () -> {
            final List<ResourcePack> updatedPacks = new LinkedList<>();

            try {
                for (ResourcePack pack : this.packs) {
                    if (pack.refreshHash().get(10, TimeUnit.SECONDS))
                        updatedPacks.add(pack);
                }

                if (!updatedPacks.isEmpty() && EnvUtils.isAutoApplyNewVersion()) {
                    final var infos = updatedPacks.stream().map(ResourcePack::getInfo).toList();
                    Bukkit.getOnlinePlayers().forEach(player -> PackUtils.applyResourcePacks(player, infos, false));
                }
            } catch (Exception e) {
                ForcePack.getPlugin(ForcePack.class).getLogger().severe("An error occurred while refreshing resource packs: " + e.getMessage());
            }
        }, 0L, 30 * 20L);
        return true;
    }

    @CanIgnoreReturnValue
    public boolean stopScheduling() {
        if (this.refreshTask != null) {
            this.refreshTask.cancel();
            this.refreshTask = null;
            return true;
        }
        return false;
    }

    public @NotNull List<ResourcePackInfoLike> getResourcePackInfos() {
        return this.packs.stream().map(ResourcePack::getInfo).toList();
    }

}
