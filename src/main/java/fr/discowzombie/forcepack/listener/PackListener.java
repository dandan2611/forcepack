package fr.discowzombie.forcepack.listener;

import fr.discowzombie.forcepack.service.ResourcePackService;
import fr.discowzombie.forcepack.utils.PackUtils;
import net.kyori.adventure.resource.ResourcePackInfoLike;
import net.kyori.adventure.resource.ResourcePackRequest;
import net.kyori.adventure.resource.ResourcePackStatus;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerResourcePackStatusEvent;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;

public final class PackListener implements Listener {

    @EventHandler
    private void onJoin(final PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        final ResourcePackService resourcePackService =
                Objects.requireNonNull(Bukkit.getServicesManager().load(ResourcePackService.class)); // Dynamically load resource pack service, so if changed, the new one will be used.
        final var infos = resourcePackService.getResourcePackInfos();

        PackUtils.applyResourcePacks(player, infos, true);
    }

}
